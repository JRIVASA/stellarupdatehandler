Attribute VB_Name = "Global"
Public TmpForm                      As FrmProceso
Public mCn                          As ADODB.Connection
Public mCnStr                       As String
Public Link                         As Object
Public DebugVBApp                   As Boolean

Public SharedCls                    As Object
Public EnProceso                    As Boolean
Public TmpUltimosDatos              As Dictionary
Public Configuracion                As Dictionary
Public Inicializado                 As Boolean
Public EnProcesoDeInicializacion    As Boolean

Public Const SWP_NOSIZE = &H1
Public Const SWP_NOMOVE = &H2
Public Const SWP_NOZORDER = &H4
Public Const SWP_NOREDRAW = &H8
Public Const SWP_NOACTIVATE = &H10
Public Const SWP_FRAMECHANGED = &H20        '  frame change send WM_NCCALCSIZE
Public Const SWP_SHOWWINDOW = &H40
Public Const SWP_HIDEWINDOW = &H80
Public Const SWP_NOCOPYBITS = &H100
Public Const SWP_NOOWNERZORDER = &H200      '  No use propietary Z order

Public Const SWP_DRAWFRAME = SWP_FRAMECHANGED
Public Const SWP_NOREPOSITION = SWP_NOOWNERZORDER

Public Const HWND_TOP = 0
Public Const HWND_BOTTOM = 1
Public Const HWND_TOPMOST = -1
Public Const HWND_NOTOPMOST = -2

Public Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, _
ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, _
ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long

Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" ( _
ByVal hWnd As Long, _
ByVal lpOperation As String, _
ByVal lpFile As String, _
ByVal lpParameters As String, _
ByVal lpDirectory As String, _
ByVal nShowCmd As Long) As Long

'**************************************
'Windows API/Global Declarations for :Changing priority
'**************************************

Private Const NORMAL_PRIORITY_CLASS = &H20
Private Const IDLE_PRIORITY_CLASS = &H40
Private Const HIGH_PRIORITY_CLASS = &H80
Private Const REALTIME_PRIORITY_CLASS = &H100
Private Const PROCESS_DUP_HANDLE = &H40

Private Const SYNCHRONIZE = &H100000
Private Const INFINITE = -1&

Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, _
ByVal bInheritHandle As Long, ByVal dwProcessID As Long) As Long
Private Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long

Public AppProcId As Long

Public Declare Function GetCurrentProcessId Lib "kernel32" () As Long

Public Declare Sub CloseHandle Lib "kernel32" _
(ByVal hPass As Long)

Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Public Sub Main()
    If App.PrevInstance Then
        End
    End If
    Set Configuracion = New Dictionary
'    Configuracion("Accion") = "CHK_UPD_BUSINESS"
'    Configuracion("CnProvider") = "SQLOLEDB.1"
'    SharedCls.Inicializar
End Sub

Public Sub IniciarProceso()
    If Not Inicializado And Not EnProcesoDeInicializacion Then
        EnProcesoDeInicializacion = True
        Set TmpForm = New FrmProceso
        Load TmpForm
    End If
End Sub

Public Sub DetenerProceso()
    If Inicializado Then
        'TmpForm.LectorContinuo.Enabled = False
        ' NO HACER ESTE CICLO... CUELGA EL CALLER APP.
        'If EnProceso Then
            'While EnProceso
                'DoEvents
            'Wend
        'End If
        Set TmpForm = Nothing
        Inicializado = False
    End If
End Sub

Public Function Unzip(ByVal ZipFilePath As String, ByVal TargetPath As String) As Boolean
    
    On Error GoTo Error
    
    Link.CreateFullDirectoryPath TargetPath
    
    Dim ShellClass  As Shell32.Shell
    Dim FileSource  As Shell32.Folder
    Dim FileDest    As Shell32.Folder
    Dim FolderItems As Shell32.FolderItems
    
    Set ShellClass = New Shell32.Shell
    Set FileSource = ShellClass.NameSpace(ZipFilePath)
    Set FileDest = ShellClass.NameSpace(TargetPath)
    Set FolderItems = FileSource.Items
    
    Call FileDest.CopyHere(FolderItems, 20)
          
    Unzip = True
          
    Exit Function
    
Error:
    
End Function

' Start the indicated program and wait for it
' to finish, hiding while we wait.

Public Function ShellAndWait(ByVal PathName As String, _
Optional ByVal WindowStyle As VbAppWinStyle = vbMinimizedFocus) As Double
    
    Dim Process_Id As Long
    Dim Process_Handle As Long
    
    Process_Id = Shell(PathName, WindowStyle)

    DoEvents

    ' Wait for the program to finish.
    ' Get the process handle.
    
    Process_Handle = OpenProcess(SYNCHRONIZE, 0, Process_Id)
    
    If Process_Handle <> 0 Then
        WaitForSingleObject Process_Handle, INFINITE
        CloseHandle Process_Handle
    End If
    
End Function

Public Sub WaitForProcess(ByVal ProcID As Long)
    
    On Error GoTo Error
    
    Dim Process_Id As Long: Process_Id = ProcID
    Dim Process_Handle As Long

    DoEvents

    ' Wait for the program to finish.
    ' Get the process handle.
    
    Process_Handle = OpenProcess(SYNCHRONIZE, 0, Process_Id)
    
    If Process_Handle <> 0 Then
        WaitForSingleObject Process_Handle, INFINITE
        CloseHandle Process_Handle
    End If
    
    Exit Sub
    
Error:
    
End Sub

Public Function Actualizar_Aplicacion(ByVal pProceso As String, _
ByVal pAplicacion As String, _
Optional ByVal pControlID As Boolean = False, _
Optional ByVal pIDProceso As Long, _
Optional ByVal pTiempoEsperaenMilisegundos As Long = 2000) As Boolean
    
    On Error GoTo Errores
    
    If DebugVBApp Then Exit Function
    ' Para que no me tumbe el Visual Studio si estoy debuggeando.
    ' Esta variable la lleno al inicio del programa.
    
    '---------------------------------------------'
    ' FUNCION PARA EL REINICIO DE UNA APLICACION  '
    '                                             '
    ' UTILIZA UN VBSCRIPT QUE SE CREA, SE EJECUTA '
    ' Y Espera A LA FINALIZACION DE LA APLICACION '
    ' PARA VOLVERLE A EJECUTAR                    '
    '---------------------------------------------'
    
    Dim TmpArchivo As String
    
    Dim TmpScript As Object
    Dim ObjCreateScript As Object
    
    TmpArchivo = App.Path & "\TmpUpdateApplication.vbs"
    
    KillSecure TmpArchivo
    
    Set TmpScript = CreateObject("Scripting.FileSystemObject")
    
    Set ObjCreateScript = TmpScript.CreateTextFile(TmpArchivo, False)
    
    With ObjCreateScript
        
        .WriteLine "Option Explicit"
        .WriteLine "'-------------------------------------'"
        .WriteLine "' VBSCRIPT QUE REINICIA LA APLICACION '"
        .WriteLine "' '" & pAplicacion & "'        '"
        .WriteLine "' CUANDO ESTA SE CIERRA               '"
        .WriteLine "'-------------------------------------'"
        .WriteLine "'"
        .WriteLine "Const strComputer = ""."" "
        .WriteLine "'"
        .WriteLine "Dim objWMIService"
        .WriteLine "Dim colProcessList"
        .WriteLine "Dim bolL_Proceso_Terminado"
        .WriteLine "Dim objProcess"
        .WriteLine "Dim WshShell"
        .WriteLine "Dim oExec"
        .WriteLine "'"
        .WriteLine "Set objWMIService = GetObject(""winmgmts:"" & ""{impersonationLevel=impersonate}!\\"" & strComputer & ""\root\cimv2"")"
        .WriteLine "'"
        .WriteLine "Do"
        .WriteLine "    Set colProcessList = objWMIService.ExecQuery(""SELECT * FROM Win32_Process WHERE Name = '" & pProceso & "'"")"
        .WriteLine "    bolL_Proceso_Terminado = true"
        .WriteLine "    '"
        .WriteLine "    For Each objProcess in colProcessList"
        
        If (pControlID = False) Then
            .WriteLine "        WScript.Echo ""�ENCONTRADO EL PROCESO '" & pProceso & "' !"""
            .WriteLine "        bolL_Proceso_Terminado = True"
        Else
            .WriteLine "        If (objProcess.ProcessId = " & pIDProceso & ") Then"
            .WriteLine "            WScript.Echo ""�ENCONTRADO EL PROCESO '" & pProceso & "' ID:" & pIDProceso & " !"""
            .WriteLine "            bolL_Proceso_Terminado = True"
            .WriteLine "        End If"
        End If
        
        .WriteLine "        "
        .WriteLine "    Next"
        .WriteLine "    "
        .WriteLine "Loop While bolL_Proceso_Terminado = False"
        .WriteLine "'"
        .WriteLine "' EL PROCESO O TERMINO O NO EXIST�A"
        .WriteLine "' ENTONCES LO VOLVEMOS A EJECUTAR"
        .WriteLine "'"
        .WriteLine "Set WshShell = WScript.CreateObject(""WScript.Shell"")"
        
        .WriteLine "Set oExec = WshShell.Exec(""TASKKILL /F /IM """"" & App.EXEName & ".exe"""""")"
        
        If pControlID Then
            .WriteLine "Set oExec = WshShell.Exec(""TASKKILL /F /PID " & pIDProceso & """)"
        Else
            .WriteLine "Set oExec = WshShell.Exec(""TASKKILL /F /IM """"" & pProceso & """"""")"
        End If
        
        .WriteLine "WScript.Sleep " & pTiempoEsperaenMilisegundos & " ' NO SATUREMOS EL SISTEMA"
        
        '.WriteLine "Set oExec = WshShell.Exec(""START """" /WAIT """ & Link.oResp("PackageDirPath") & "Lite-Setup.exe"""" /S"")"
        '.WriteLine "Set oExec = WshShell.Exec(""""""" & Link.oResp("PackageDirPath") & "Lite-Setup.exe"""" /S"")"
        .WriteLine "WshShell.Run """"""" & Link.oResp("PackageDirPath") & "Lite-Setup.exe"""" /S"", 1, 1"
        
        '.WriteLine "Set oExec = WshShell.Exec(""START """" /WAIT """ & App.Path & "\Messages\SendMessage_RequiresParams.exe"" ""[StellarInstallerExternalProgressID SAFE APP CLOSE]"")"
        '.WriteLine "Set oExec = WshShell.Exec(""""""" & App.Path & "\Messages\SendMessage_RequiresParams.exe"""" [StellarInstallerExternalProgressID SAFE APP CLOSE]"")"
        .WriteLine "Set oExec = WshShell.Exec(""""""" & App.Path & "\Messages\SendMessage_RequiresParams.exe"""" [StellarInstallerExternalProgressID SAFE APP CLOSE]"")"
        .WriteLine "WScript.Sleep 100"
        
        '.WriteLine "Set oExec = WshShell.Exec(""START """" /WAIT """ & App.Path & "\Messages\Progress.exe"" ""|p|Title=Stellar BUSINESS AutoUpdate|p|Subtitle=Finalizando Instalacion"")"
        '.WriteLine "Set oExec = WshShell.Exec(""""""" & App.Path & "\Messages\Progress.exe"""" |p|Title=Stellar BUSINESS AutoUpdate|p|Subtitle=Finalizando Instalacion"")"
        .WriteLine "Set oExec = WshShell.Exec(""""""" & App.Path & "\Messages\Progress.exe"""" |p|Title=Stellar BUSINESS AutoUpdate|p|Subtitle=...Finalizando Instalacion..."")"
        .WriteLine "WScript.Sleep 5000"
        
        .WriteLine "Set oExec = WshShell.Exec(""" & pAplicacion & """)"
        .WriteLine "'WScript.Echo ""�ARRANQUE DE LA APLICACION '" & pAplicacion & "' FINALIZADO!"""
        
        '.WriteLine "Set oExec = WshShell.Exec(""START """" /WAIT """ & App.Path & "\Messages\SendMessage_RequiresParams.exe"" ""[StellarInstallerExternalProgressID SAFE APP CLOSE]"")"
        '.WriteLine "Set oExec = WshShell.Exec(""""""" & App.Path & "\Messages\SendMessage_RequiresParams.exe"""" [StellarInstallerExternalProgressID SAFE APP CLOSE]"")"
        .WriteLine "Set oExec = WshShell.Exec(""""""" & App.Path & "\Messages\SendMessage_RequiresParams.exe"""" [StellarInstallerExternalProgressID SAFE APP CLOSE]"")"
        
        .Close
        
    End With
    
    Set ObjCreateScript = Nothing
    Set TmpScript = Nothing
    
    Shell "cscript """ & TmpArchivo & """"
    
    Actualizar_Aplicacion = True
    
    Exit Function
    
Errores:
    
    Err.Clear
    
End Function

Public Function Reinicio_Aplicacion(ByVal pProceso As String, _
ByVal pAplicacion As String, _
Optional ByVal pControlID As Boolean = False, _
Optional ByVal pIDProceso As Long, _
Optional ByVal pTiempoEsperaenMilisegundos As Long = 2000) As Boolean
    
    On Error GoTo Errores
    
    If DebugVBApp Then Exit Function
    ' Para que no me tumbe el Visual Studio si estoy debuggeando.
    ' Esta variable la lleno al inicio del programa.
    
    '---------------------------------------------'
    ' FUNCION PARA EL REINICIO DE UNA APLICACION  '
    '                                             '
    ' UTILIZA UN VBSCRIPT QUE SE CREA, SE EJECUTA '
    ' Y Espera A LA FINALIZACION DE LA APLICACION '
    ' PARA VOLVERLE A EJECUTAR                    '
    '---------------------------------------------'
    
    Dim TmpArchivo As String
    
    Dim TmpScript As Object
    Dim ObjCreateScript As Object
    
    TmpArchivo = App.Path & "\TmpRestartApplication.vbs"
    
    If (Dir(TmpArchivo) <> "") Then Kill (TmpArchivo)
    
    Set TmpScript = CreateObject("Scripting.FileSystemObject")
    
    Set ObjCreateScript = TmpScript.CreateTextFile(TmpArchivo, False)
    
    With ObjCreateScript
        
        .WriteLine "Option Explicit"
        .WriteLine "'-------------------------------------'"
        .WriteLine "' VBSCRIPT QUE REINICIA LA APLICACION '"
        .WriteLine "' '" & pAplicacion & "'        '"
        .WriteLine "' CUANDO ESTA SE CIERRA               '"
        .WriteLine "'-------------------------------------'"
        .WriteLine "'"
        .WriteLine "Const strComputer = ""."" "
        .WriteLine "'"
        .WriteLine "Dim objWMIService"
        .WriteLine "Dim colProcessList"
        .WriteLine "Dim bolL_Proceso_Terminado"
        .WriteLine "Dim objProcess"
        .WriteLine "Dim WshShell"
        .WriteLine "Dim oExec"
        .WriteLine "'"
        .WriteLine "Set objWMIService = GetObject(""winmgmts:"" & ""{impersonationLevel=impersonate}!\\"" & strComputer & ""\root\cimv2"")"
        .WriteLine "'"
        .WriteLine "Do"
        .WriteLine "    Set colProcessList = objWMIService.ExecQuery(""SELECT * FROM Win32_Process WHERE Name = '" & pProceso & "'"")"
        .WriteLine "    bolL_Proceso_Terminado = true"
        .WriteLine "    '"
        .WriteLine "    For Each objProcess in colProcessList"
        
        If (pControlID = False) Then
            .WriteLine "        WScript.Echo ""�ENCONTRADO EL PROCESO '" & pProceso & "' !"""
            .WriteLine "        bolL_Proceso_Terminado = True"
        Else
            .WriteLine "        If (objProcess.ProcessId = " & pIDProceso & ") Then"
            .WriteLine "            WScript.Echo ""�ENCONTRADO EL PROCESO '" & pProceso & "' ID:" & pIDProceso & " !"""
            .WriteLine "            bolL_Proceso_Terminado = True"
            .WriteLine "        End If"
        End If
        
        .WriteLine "        "
        .WriteLine "    Next"
        .WriteLine "    "
        .WriteLine "Loop While bolL_Proceso_Terminado = False"
        .WriteLine "'"
        .WriteLine "' EL PROCESO O TERMINO O NO EXIST�A"
        .WriteLine "' ENTONCES LO VOLVEMOS A EJECUTAR"
        .WriteLine "'"
        .WriteLine "Set WshShell = WScript.CreateObject(""WScript.Shell"")"
        
        If pControlID Then
            .WriteLine "Set oExec = WshShell.Exec(""TASKKILL /F /PID " & pIDProceso & """)"
        Else
            .WriteLine "Set oExec = WshShell.Exec(""TASKKILL /F /IM " & pProceso & """)"
        End If
        
        .WriteLine "WScript.Sleep " & pTiempoEsperaenMilisegundos & " ' NO SATUREMOS EL SISTEMA"
        .WriteLine "Set oExec = WshShell.Exec(""" & pAplicacion & """)"
        .WriteLine "'WScript.Echo ""�ARRANQUE DE LA APLICACION '" & pAplicacion & "' FINALIZADO!"""
        
        .Close
        
    End With
    
    Set ObjCreateScript = Nothing
    Set TmpScript = Nothing
    
    Shell "cscript """ & TmpArchivo & """"
    
    Reinicio_Aplicacion = True
    
    Exit Function
    
Errores:
    
    Err.Clear
    
End Function

Public Function ShellEx(ByVal hWnd As Long, ByVal lpOperation As String, _
ByVal lpFile As String, ByVal lpParameters As String, _
ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
    ShellEx = ShellExecute(hWnd, lpOperation, lpFile, lpParameters, lpDirectory, nShowCmd)
End Function

Public Function KillSecure(ByVal pFile As String) As Boolean
    On Error GoTo KS
    Kill pFile: KillSecure = True
    Exit Function
KS:
    'Debug.Print Err.Description
    Err.Clear 'Ignorar.
End Function

Public Function KillShot(ByVal pFile As String) As Boolean
    KillShot = KillSecure(pFile)
End Function

Public Function ReadFileIntoString(StrFilePath As String) As String
    
    On Error GoTo ErrFile

    Dim FSObject As New FileSystemObject
    Dim TStream As TextStream

    Set TStream = FSObject.OpenTextFile(StrFilePath)
    If Not TStream.AtEndOfStream Then ReadFileIntoString = TStream.ReadAll

    Exit Function
    
ErrFile:
    
    ReadFileIntoString = ""
    
End Function
