VERSION 5.00
Begin VB.Form FrmProceso 
   ClientHeight    =   960
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   1740
   Icon            =   "FrmLectura.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   960
   ScaleWidth      =   1740
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin StellarUpdateHandler.SimpleDownloader Downloader 
      Height          =   315
      Left            =   960
      Top             =   300
      Width           =   315
      _ExtentX        =   556
      _ExtentY        =   556
   End
   Begin VB.Timer LectorContinuo 
      Enabled         =   0   'False
      Left            =   300
      Top             =   240
   End
End
Attribute VB_Name = "FrmProceso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Downloader_Complete(URL As String, Data As String, Key As String)
    
    On Error GoTo Error
    
    Dim GoodDownload As Boolean
    
    Dim intFF As Integer, bytData() As Byte
    
    GoodDownload = Len(Data) > 0
    
    If GoodDownload Then
        
        If Len(Downloader.Tag) > 0 Then
            
            SpliTag = Split(Downloader.Tag, "|")
            
            If UBound(SpliTag) = 1 Then
            
                BytesDone = SpliTag(0)
                BytesTotal = SpliTag(1)
                
                GoodDownload = (Val(BytesDone) >= Val(BytesTotal))
            
            Else
                
                GoodDownload = True
                
            End If
        
        Else
            
            GoodDownload = True
            
        End If
        
    End If
    
    If GoodDownload Then
        
        'Me.Caption = URL
        Debug.Print URL
        ' get a file number
        intFF = FreeFile
        ' place in a byte array
        bytData = Data
        ' open for binary access so every byte is saved the way it is
        
        TempDir = Link.oResp("TempDir")
        TempDir = TempDir & Link.oResp("Version_FileName")
        
        Open TempDir For Binary Access Write As #intFF
            Put #intFF, , bytData
        Close #intFF
        
        Link.oResp("Download_Success") = True
        
    Else
        
        Link.oResp("Download_Success") = False
        
    End If
    
Finally:
    
    EnProceso = False
    
    If GoodDownload Then
        Link.GetMessage "CheckDownload"
    Else
        Link.GetMessage "DownloadError"
    End If
    
    Exit Sub
    
Error:
    
    Reset
    
    Link.oResp("Download_Success") = False
    
    GoTo Finally
    
End Sub

Private Sub Downloader_Progress(URL As String, Key As String, _
ByVal BytesDone As Double, ByVal BytesTotal As Double, _
ByVal Status As AsyncStatusCodeConstants)
    
    If BytesTotal > 0 Then
        
        Downloader.Tag = BytesDone & "|" & BytesTotal
        
        On Error Resume Next
        
        Static Hora As Double
        
        If Hora = 0 Then Hora = Timer
        
        If (Timer >= (Hora + 1)) Then
            Hora = Timer
            Link.oResp.Item("DownloadProgressInfo") = Fix(BytesDone / 1024) & " KB" & " / " & Fix(BytesTotal / 1024) & " KB" & " " & "(" & FormatNumber((BytesDone / BytesTotal) * 100) & "%" & ")"
            Link.GetMessage "DownloadProgressInfo"
            Exit Sub
        End If
        
    Else
        Downloader.Tag = Empty
    End If
    
    If Status = vbAsyncStatusCodeError Then
        Link.oResp("Download_Success") = False
        Link.GetMessage "CheckDownload"
        Exit Sub
    End If
    
End Sub

Private Sub Form_Load()
    
    On Error GoTo Error
    
    If Not Inicializado Then
        
        Inicializado = True
        
        LectorContinuo.Interval = 100
        LectorContinuo.Enabled = True
        
    End If
    
    EnProcesoDeInicializacion = False
    
    Exit Sub
    
Error:
    
    EnProcesoDeInicializacion = False
    Inicializado = False
    
    End
    
End Sub

Private Function gt1(Optional ByVal gkhkfhkfd)
    If IsMissing(gkhkfhkfd) Then
        gt1 = Chr(69)
    Else
        gt1 = Left(Right(Replace("XAXEXXC", "X", Empty), 2), 1)
    End If
End Function

Public Sub LectorContinuo_Timer()
    
    If EnProceso Then
        ' NO HACER ESTE CICLO... CUELGA EL CALLER APP.
        'While EnProceso
            'DoEvents
        'Wend
        Exit Sub
    Else
        
        EnProceso = True
        
        If UCase(Configuracion("Accion")) = "CHK_UPD_BUSINESS" Then
            
            On Error GoTo Err_Chk_Upd_Business
            
            LectorContinuo.Enabled = False
            
            mAllowCheckOnline = Val(Link.BuscarReglaNegocioStr("BUSINESS_CheckUpdates_Online", "1")) = 1
            
            If mAllowCheckOnline Then
                
                On Error GoTo Err_Chk_Upd_Business_Online
                
                If Configuracion.Exists("TestMode") Then
                    Link.oResp("Version") = "2.9.14"
                    Link.oResp("Version_Fecha") = Now
                    Link.oResp("Version_DocURL") = "http://..../BUSINESS v2.9.14.pdf"
                    Link.oResp("Version_FileURL") = "http://..../BUSINESS v2.9.14.zip"
                    Link.oResp("TempDir") = IIf(Right(Environ("Temp"), 1) = "\", Environ("Temp"), Environ("Temp") & "\")
                    Link.GetMessage "CheckVersion"
                Else
                    Configuracion("Accion_Estatus") = "Conectando"
                    
                    mCnStr = Chr(80) & Chr(114) & Chr(111) & Chr(118) & Chr(105) & Chr(100) & Chr(101) & Chr(114) & _
                    Chr(61) & Chr(77) & Chr(83) & Chr(68) & Chr(97) & Chr(116) & Chr(97) & Chr(83) & _
                    Chr(104) & Chr(97) & Chr(112) & Chr(101) & Chr(46) & Chr(49) & Chr(59) & Chr(68) & Chr(97) & _
                    Chr(116) & Chr(97) & Chr(32) & Chr(80) & Chr(114) & Chr(111) & Chr(118) & _
                    Chr(105) & Chr(100) & Chr(101) & Chr(114) & Chr(61) & Chr(83) & Chr(81) & _
                    Chr(76) & Chr(79) & Chr(76) & Chr(69) & Chr(68) & Chr(66) & Chr(46) & Chr(49) & _
                    Chr(59) & Chr(68) & Chr(97) & Chr(116) & Chr(97) & Chr(32) & Chr(83) & Chr(111) & _
                    Chr(117) & Chr(114) & Chr(99) & Chr(101) & Chr(61) & "a2nwplsk14sql-v05.shr.prod.iad2.secureserver.net" & Chr(59) & Chr(73) & Chr(110) & Chr(105) & Chr(116) & Chr(105) & _
                    Chr(97) & Chr(108) & Chr(32) & Chr(67) & Chr(97) & Chr(116) & Chr(97) & Chr(108) & _
                    Chr(111) & Chr(103) & Chr(61) & Chr(66) & "I" & Chr(71) & "W" & _
                    Chr((146 / 2)) & "S" & gt1 & "S" _
                    & "O" & Left$(Split("PM", "|")(0), 1) & Chr(79) & "R" & "T" & gt1(True) & ";" & _
                    Chr(80) & Chr(101) & Chr(114) & Chr(115) & Chr(105) & Chr(115) & Chr(116) & Chr(32) _
                    & Chr(83) & Chr(101) & Chr(99) & Chr(117) & Chr(114) & Chr(105) & Chr(116) & _
                    Chr(121) & Chr(32) & Chr(73) & Chr(110) & Chr(102) & Chr(111) & Chr(61) & _
                    Chr(70) & Chr(97) & Chr(108) & Chr(115) & Chr(101) & Chr(59) & Chr(85) & _
                    Chr(115) & Chr(101) & Chr(114) & Chr(32) & Chr(73) & Chr(68) & Chr(61) & Chr(66) & "I" & _
                    Chr(71) & UCase("w") & Chr((37 * 2) - 1) & "S" & gt1("fdjjk") & UCase("s") _
                    & "O" & Split("P|M", "|")(0) & UCase(Chr(121 - 12 + 2)) & "R" & Chr(84) & gt1("xY") & Chr(59) & Chr(80) & Chr(97) & Chr(115) & _
                    Chr(115) & Chr(119) & Chr(111) & Chr(114) & Chr(100) & Chr(61) & Chr(82) & (3 - 2 + 1 - 1) & _
                    "3" & (1 / 1 * 1) & (Val("") ^ 0) & (5434 * 0) & Int(9 - 9) & LCase(Chr(65)) & Chr(59)
                    
                    'MsgBox mCnStr
                    
                    Set mCn = New ADODB.Connection
                    
                    mCn.ConnectionString = mCnStr
                    mCn.Open , , Options:=adAsyncConnect
                    
                    TmpTime = Timer()
                    
                    Do Until (Timer() - TmpTime) > 30 _
                    Or mCn.State = adStateOpen
                        DoEvents
                    Loop
                    
                    If mCn.State <> adStateOpen Then
                        Configuracion("Accion_Estatus") = "Fallo"
                        Configuracion("Accion_Error") = "ConnectionTimeout"
                        Err.Raise 999, , Configuracion("Accion_Error")
                    End If
                    
                    mCnStr = Empty
                    
                    Configuracion("Accion_Estatus") = "Consultando"
                    
                    Set mRs = mCn.Execute("SELECT TOP 1 * FROM DESCARGAS " & _
                    "WHERE 1 = 1 AND cProducto = 1 AND bDescargaFija = 0 " & _
                    "ORDER BY cFecha DESC")
                    
                    Set mRs.ActiveConnection = Nothing
                    
                    mCn.Close
                    
                    If Not mRs.EOF Then
                        
                        Link.oResp("Version") = mRs!cVersion
                        Link.oResp("Version_Fecha") = mRs!cFecha
                        Link.oResp("Version_FileURL") = mRs!cRuta1
                        Link.oResp("Version_DocURL") = mRs!cRuta2
                        Link.oResp("TempDir") = IIf(Right(Environ("Temp"), 1) = "\", Environ("Temp"), Environ("Temp") & "\")
                        
                        Link.ExecuteSafeSQL "IF NOT EXISTS(SELECT * FROM MA_VERSION WHERE Version = '" & mRs!cVersion & "') " & _
                        " INSERT INTO MA_VERSION (Version, Actualizacion, NombreArchivo) " & _
                        "VALUES ('" & Link.QuitarComillasSimples(mRs!cVersion) & "', '" & Link.FechaBD(mRs!cFecha) & "', " & _
                        "'" & Link.QuitarComillasSimples(mRs!cRuta1) & "') " & _
                        "ELSE " & _
                        "UPDATE MA_VERSION SET NombreArchivo = '" & Link.QuitarComillasSimples(mRs!cRuta1) & "' " & _
                        "WHERE NombreArchivo = '' AND Version = '" & Link.QuitarComillasSimples(mRs!cVersion) & "' ", _
                        Link.CnADM, RowsAffected
                        
                        Link.oResp("mSharedDir") = Link.BuscarReglaNegocioStr("BUSINESS_SharedUpdateFolder", Empty)
                        If Len(Trim(Link.oResp("mSharedDir"))) > 0 Then
                            Link.oResp("mSharedFilePath") = Link.FindPath(Link.oResp("Version_FileURL"), -1, Link.oResp("mSharedDir"))
                            Link.oResp("mTempFileName") = Replace(Link.oResp("Version_FileURL"), _
                            Link.GetDirParent(Link.oResp("Version_FileURL"), True) & "/", Empty)
                            Link.oResp("mTempFilePath") = Link.oResp("TempDir") & Link.oResp("mTempFileName")
                            Link.oResp("mSharedFileExists") = Link.PathExists(Link.oResp("mSharedFilePath"))
                            Link.oResp("mTempFileExists") = Link.PathExists(Link.oResp("mTempFilePath"))
                            If Link.oResp("mSharedFileExists") _
                            And Not Link.oResp("mTempFileExists") Then
                                Link.CopyPath Link.oResp("mSharedFilePath"), Link.oResp("mTempFilePath")
                            ElseIf Link.oResp("mSharedFileExists") _
                            And Link.oResp("mTempFileExists") Then
                                If FileLen(Link.oResp("mSharedFilePath")) <> FileLen(Link.oResp("mTempFilePath")) Then
                                    Link.KillSecure Link.oResp("mTempFilePath")
                                    Link.CopyPath Link.oResp("mSharedFilePath"), Link.oResp("mTempFilePath")
                                End If
                            End If
                            Link.oResp("AllowSharedDir") = True
                        Else
                            Link.oResp("AllowSharedDir") = False
                        End If
                        
                        EnProceso = False
                        Link.oResp("OnlineCheck") = True
                        Link.GetMessage "CheckVersion"
                    Else
                        EnProceso = False
                        Link.GetMessage "NoData"
                    End If
                    
Err_Chk_Upd_Business_Online:
                    
                    If Err.Number <> 0 Then
                        Resume FallbackLANCheck
                    End If
                    
                End If
                
            Else
                
FallbackLANCheck:
                
                On Error GoTo Err_Chk_Upd_Business
                
                Link.oResp("mSharedDir") = Link.BuscarReglaNegocioStr("BUSINESS_SharedUpdateFolder", Empty)
                
                If Len(Trim(Link.oResp("mSharedDir"))) > 0 Then
                    Link.oResp("AllowSharedDir") = True
                Else
                    Link.oResp("AllowSharedDir") = False
                End If
                
                If Link.oResp("AllowSharedDir") Then
                    
                    Set mRs = Link.CnADM.Execute("SELECT TOP 1 * FROM MA_VERSION WHERE LEN(LTRIM(RTRIM(NombreArchivo))) > 0 ORDER BY Actualizacion DESC")
                    
                    If Not (mRs.EOF And mRs.BOF) Then
                        
                        Link.oResp("Version") = mRs!Version
                        Link.oResp("Version_Fecha") = mRs!Actualizacion
                        Link.oResp("Version_FileURL") = mRs!NombreArchivo
                        Link.oResp("Version_DocURL") = vbNullString
                        Link.oResp("TempDir") = IIf(Right(Environ("Temp"), 1) = "\", Environ("Temp"), Environ("Temp") & "\")
                        
                        Link.oResp("mSharedFilePath") = Link.FindPath(Link.oResp("Version_FileURL"), -1, Link.oResp("mSharedDir"))
                        Link.oResp("mTempFileName") = Replace(Link.oResp("Version_FileURL"), _
                        Link.GetDirParent(Link.oResp("Version_FileURL")) & "\", Empty)
                        Link.oResp("mTempFilePath") = Link.oResp("TempDir") & Link.oResp("mTempFileName")
                        Link.oResp("mSharedFileExists") = Link.PathExists(Link.oResp("mSharedFilePath"))
                        Link.oResp("mTempFileExists") = Link.PathExists(Link.oResp("mTempFilePath"))
                        If Link.oResp("mSharedFileExists") _
                        And Not Link.oResp("mTempFileExists") Then
                            Link.CopyPath Link.oResp("mSharedFilePath"), Link.oResp("mTempFilePath")
                        ElseIf Link.oResp("mSharedFileExists") _
                        And Link.oResp("mTempFileExists") Then
                            If FileLen(Link.oResp("mSharedFilePath")) <> FileLen(Link.oResp("mTempFilePath")) Then
                                Link.KillSecure Link.oResp("mTempFilePath")
                                Link.CopyPath Link.oResp("mSharedFilePath"), Link.oResp("mTempFilePath")
                            End If
                        End If
                        
                        EnProceso = False
                        
                        Link.oResp("OnlineCheck") = False
                        Link.GetMessage "CheckVersion"
                        
                    Else
                        EnProceso = False
                        Link.GetMessage "NoData"
                    End If
                    
                Else
                    EnProceso = False
                    Link.GetMessage "NoData"
                End If
                
            End If
            
Err_Chk_Upd_Business:
            
            If Err.Number <> 0 Then
                
                mErrorNumber = Err.Number
                mErrorDesc = Err.Description
                mErrorSource = Err.Source
                
                Resume ClearErr1
ClearErr1:
                On Error Resume Next
                
                Link.ShowToast Link.GetLines & " Error al comprobar si existe una " & _
                "nueva versión, información adicional: " & _
                mErrorDesc & Link.GetLines & mErrorNumber & Link.GetLines & _
                mErrorSource & Link.GetLines, 6000, 12500, , 1, _
                &H9E5300, vbWhite, &H9E5300, Link.GetFont("Tahoma", "9", True)
                
                End
                
            End If
            
            EnProceso = False
            
        ElseIf UCase(Configuracion("Accion")) = "UPDATE_BUSINESS" Then
            
            On Error GoTo Err_Upd_Business
            
            LectorContinuo.Enabled = False
            
            Dim Auth As Boolean
            
            If Configuracion("TestMode") Then
                Auth = True
            Else
                
                mCnStr = Chr(80) & Chr(114) & Chr(111) & Chr(118) & Chr(105) & Chr(100) & Chr(101) & Chr(114) & _
                Chr(61) & Chr(77) & Chr(83) & Chr(68) & Chr(97) & Chr(116) & Chr(97) & Chr(83) & _
                Chr(104) & Chr(97) & Chr(112) & Chr(101) & Chr(46) & Chr(49) & Chr(59) & Chr(68) & Chr(97) & _
                Chr(116) & Chr(97) & Chr(32) & Chr(80) & Chr(114) & Chr(111) & Chr(118) & _
                Chr(105) & Chr(100) & Chr(101) & Chr(114) & Chr(61) & Chr(83) & Chr(81) & _
                Chr(76) & Chr(79) & Chr(76) & Chr(69) & Chr(68) & Chr(66) & Chr(46) & Chr(49) & _
                Chr(59) & Chr(68) & Chr(97) & Chr(116) & Chr(97) & Chr(32) & Chr(83) & Chr(111) & _
                Chr(117) & Chr(114) & Chr(99) & Chr(101) & Chr(61) & "a2nwplsk14sql-v05.shr.prod.iad2.secureserver.net" & Chr(59) & Chr(73) & Chr(110) & Chr(105) & Chr(116) & Chr(105) & _
                Chr(97) & Chr(108) & Chr(32) & Chr(67) & Chr(97) & Chr(116) & Chr(97) & Chr(108) & _
                Chr(111) & Chr(103) & Chr(61) & Chr(66) & "I" & Chr(71) & "W" & _
                Chr((146 / 2)) & "S" & gt1 & "S" _
                & "O" & Left$(Split("PM", "|")(0), 1) & Chr(79) & "R" & "T" & gt1(True) & ";" & _
                Chr(80) & Chr(101) & Chr(114) & Chr(115) & Chr(105) & Chr(115) & Chr(116) & Chr(32) _
                & Chr(83) & Chr(101) & Chr(99) & Chr(117) & Chr(114) & Chr(105) & Chr(116) & _
                Chr(121) & Chr(32) & Chr(73) & Chr(110) & Chr(102) & Chr(111) & Chr(61) & _
                Chr(70) & Chr(97) & Chr(108) & Chr(115) & Chr(101) & Chr(59) & Chr(85) & _
                Chr(115) & Chr(101) & Chr(114) & Chr(32) & Chr(73) & Chr(68) & Chr(61) & Chr(66) & "I" & _
                Chr(71) & UCase("w") & Chr((37 * 2) - 1) & "S" & gt1("fdjjk") & UCase("s") _
                & "O" & Split("P|M", "|")(0) & UCase(Chr(121 - 12 + 2)) & "R" & Chr(84) & gt1("xY") & Chr(59) & Chr(80) & Chr(97) & Chr(115) & _
                Chr(115) & Chr(119) & Chr(111) & Chr(114) & Chr(100) & Chr(61) & Chr(82) & (3 - 2 + 1 - 1) & _
                "3" & (1 / 1 * 1) & (Val("") ^ 0) & (5434 * 0) & Int(9 - 9) & LCase(Chr(65)) & Chr(59)
                
                Set mCn = New ADODB.Connection
                
                mCn.ConnectionString = mCnStr
                mCn.Open , , Options:=adAsyncConnect
                
                TmpTime = Timer()
                
                Do Until (Timer() - TmpTime) > 30 _
                Or mCn.State = adStateOpen
                    DoEvents
                Loop
                
                If mCn.State <> adStateOpen Then
                    Configuracion("Accion_Estatus") = "Fallo"
                    Configuracion("Accion_Error") = "ConnectionTimeout"
                    Err.Raise 999, , Configuracion("Accion_Error")
                End If
                
                mCnStr = Empty
                
                Configuracion("Accion_Estatus") = "Validando"
                
                Set mRs = mCn.Execute( _
                "SELECT TOP 1* FROM CLIENTES_USUARIOS CU " & _
                "INNER JOIN CLIENTES_CONTACTO CC ON CU.cContacto = CC.cCodigo " & _
                "INNER JOIN CLIENTES C ON C.cCodigo = CC.cCliente " & _
                "INNER JOIN CLIENTES_SERVICIO CS ON C.cServicio = CS.cCodigo " & _
                "INNER JOIN CLIENTES_SERVICIO_DETALLES CSD ON CS.cCodigo = CSD.cCodigo " & _
                "AND CSD.AplicaCargos = 1 " & _
                "WHERE 1 = 1 " & _
                "AND UPPER(CU.cUsuario) = UPPER('" & _
                Link.QuitarComillasDobles(Link.QuitarComillasSimples(Link.oResp("SCP1"))) & _
                "') " & _
                "AND UPPER(CU.cClave) = UPPER(CONVERT(VARCHAR(MAX), HashBytes('MD5', '" & _
                Link.QuitarComillasDobles(Link.QuitarComillasSimples(Link.oResp("SCP2"))) & _
                "'), 2)) " & _
                "AND CSD.AplicaCargos = 1 ")
                
                Set mRs.ActiveConnection = Nothing
                
                mCn.Close
                
                Auth = Not (mRs.EOF And mRs.BOF)
                
            End If
            
            If Auth Then
                
                Downloader.Start "http://104.238.125.95/soporte/descargas/isBUSINESS/" & _
                Link.oResp("Version_FileURL"), [Always Download]
                
                Link.oResp.Item("DownloadProgressInfo") = 0 & " KB" & " / " & "N/A" & " KB" & " " & "(" & FormatNumber(0) & "%" & ")"
                Link.GetMessage "DownloadProgressInfo"
                
            Else
                
                Link.GetMessage "CancelDownload_InvalidData"
                
            End If
            
Err_Upd_Business:
            
            If Err.Number <> 0 Then
                
                mErrorNumber = Err.Number
                mErrorDesc = Err.Description
                mErrorSource = Err.Source
                
                Resume ClearErr2
ClearErr2:
                On Error Resume Next
                
                Link.ShowToast Link.GetLines & "Error al comprobar datos, " & _
                "no se pudo iniciar la descarga, información adicional: " & _
                Link.GetLines & mErrorDesc & Link.GetLines & mErrorNumber & _
                Link.GetLines & mErrorSource & Link.GetLines, 6000, 12500, , 1, _
                &H9E5300, vbWhite, &H9E5300, Link.GetFont("Tahoma", "9", True)
                
                End
                
            End If
            
        ElseIf UCase(Configuracion("Accion")) = "PREPARE_BUSINESS_INSTALL" Then
            
            On Error GoTo Err_Prep_Business_Install
            
            LectorContinuo.Enabled = False
            
            RespFin = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
            "[StellarInstallerExternalProgressID SAFE APP CLOSE]", "", 1)
            Sleep 100
            
            RespFin = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
            "[StellarInstallerExternalProgressID SAFE APP CLOSE]", "", 1)
            Sleep 100
            
            Resp1 = ShellEx(0, "open", App.Path & "\Messages\Progress.exe", _
            "|p|Title=Stellar BUSINESS AutoUpdate|p|Subtitle=...Cargando Archivos...", "", 1)  ' Comprobando Conexion
            Sleep 300
            
            If Unzip(Link.oResp("PackageFilePath"), Link.oResp("PackageDirPath")) Then
                
                RespFin = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
                "[StellarInstallerExternalProgressID SAFE APP CLOSE]", "", 1)
                Sleep 100
                
                Resp1 = ShellEx(0, "open", App.Path & "\Messages\Progress.exe", _
                "|p|Title=Stellar BUSINESS AutoUpdate|p|Subtitle=...Verificando Componentes y Dependencias...", "", 1)  ' Comprobando Conexion
                Sleep 300
                
                KillSecure App.Path & "\ERRORLEVEL.TXT"
                
                RespCheck = ShellAndWait(App.Path & "\CheckDependencies.bat" & " " & "1", vbHide)
                
                ErrorLevel = Val(ReadFileIntoString(App.Path & "\ERRORLEVEL.TXT"))
                
                If ErrorLevel < 0 Then
                    RespCheck = ShellAndWait(App.Path & "\CheckDependencies.bat" & " " & "0", vbHide)
                    ErrorLevel = Val(ReadFileIntoString(App.Path & "\ERRORLEVEL.TXT"))
                End If
                
                Success = (ErrorLevel = 0)
                
                KillSecure App.Path & "\ERRORLEVEL.TXT"
                
                RespFin = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
                "[StellarInstallerExternalProgressID SAFE APP CLOSE]", "", 1)
                Sleep 100
                
                Resp2 = ShellEx(0, "open", App.Path & "\Messages\Progress.exe", _
                "|p|Title=Stellar BUSINESS AutoUpdate|p|Subtitle=...Aplicando Scripts Acumulativos...", "", 1)  ' Comprobando Conexion
                Link.Sleep 1500
                
                RespScript = ShellAndWait(Link.oResp("PackageDirPath") & "Internal_Use\AI.bat" & _
                " " & Link.oConfig("SC1") & " " & Link.oConfig("SC2") & " " & """" & Link.oConfig("SC3") & """", vbHide) ', vbNormalFocus)
                
                RespFin = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
                "[StellarInstallerExternalProgressID SAFE APP CLOSE]", "", 1)
                Sleep 100
                
                Resp2 = ShellEx(0, "open", App.Path & "\Messages\Progress.exe", _
                "|p|Title=Stellar BUSINESS AutoUpdate|p|Subtitle=...Ejecutando Setup de Instalacion...", "", 1)  ' Comprobando Conexion
                Link.Sleep 1500
                
                'RespScript = ShellAndWait(Link.oResp("PackageDirPath") & "Lite-Setup.exe /S", vbNormalFocus)
                
                'RespFin = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
                "[StellarInstallerExternalProgressID SAFE APP CLOSE]", "", 1)
                'Sleep 100
                
                'Resp2 = ShellEx(0, "open", App.Path & "\Messages\Progress.exe", _
                "|p|Title=Stellar BUSINESS AutoUpdate|p|Subtitle=Finalizando Instalacion", "", 1)  ' Comprobando Conexion
                'Link.Sleep 2000
                
                'RespFin = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
                "[StellarInstallerExternalProgressID SAFE APP CLOSE]", "", 1)
                'Sleep 100
                
                Actualizar_Aplicacion Link.oConfig("AppExeName"), Link.oConfig("AppFilePath"), , , 5000
                
            Else
                
                RespFin = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
                "[StellarInstallerExternalProgressID SAFE APP CLOSE]", "", 1)
                Sleep 100
                
            End If
            
Err_Prep_Business_Install:
            
            If Err.Number <> 0 Then
                
                mErrorNumber = Err.Number
                mErrorDesc = Err.Description
                mErrorSource = Err.Source
                
                Resume ClearErr3
ClearErr3:
                On Error Resume Next
                
                Link.ShowToast Link.GetLines & " Error al desempacar archivos " & _
                "o durante el proceso de instalación, información adicional: " & _
                mErrorDesc & Link.GetLines & mErrorNumber & Link.GetLines & _
                mErrorSource & Link.GetLines, 6000, 12500, , 1, _
                &H9E5300, vbWhite, &H9E5300, Link.GetFont("Tahoma", "9", True)
                
                RespFin = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
                "[StellarInstallerExternalProgressID SAFE APP CLOSE]", "", 1)
                Sleep 100
                
                RespFin = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
                "[StellarInstallerExternalProgressID SAFE APP CLOSE]", "", 1)
                Sleep 100
                
                End
                
            End If
            
            EnProceso = False
            
        End If
        
    End If
    
End Sub
