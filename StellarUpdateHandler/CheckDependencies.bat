SETLOCAL ENABLEDELAYEDEXPANSION

SET "MISSING_TOOL=1"

IF DEFINED PROGRAMW6432 (GOTO CHECK) ELSE (GOTO WIN32)

:CHECK

GOTO SQLCMD64

:WIN32

IF EXIST "%PROGRAMFILES%\Microsoft SQL Server\Client SDK\ODBC\130\Tools\Binn\SQLCMD.EXE" (
	
	SET "MISSING_TOOL=0"
	
    GOTO WIN32_CONTINUE
  
)

IF EXIST "%PROGRAMFILES%\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn\SQLCMD.EXE" (
	
	SET "MISSING_TOOL=0"
	
    GOTO WIN32_CONTINUE
  
)

IF EXIST "%PROGRAMFILES%\Microsoft SQL Server\110\Tools\Binn\SQLCMD.EXE" (

  SET "MISSING_TOOL=0"
  
  GOTO WIN32_CONTINUE

)

IF EXIST "%PROGRAMFILES%\Microsoft SQL Server\100\Tools\Binn\SQLCMD.EXE" (

  SET "MISSING_TOOL=0"
  
  GOTO WIN32_CONTINUE

)

IF %1 == 1 (
REM /L*V "%~dp02014ODBC32Bits.log"
START /WAIT "" msiexec.exe /i "%~dp02014ODBC32Bits.msi" /quiet /passive /qb /norestart IACCEPTMSODBCSQLLICENSETERMS=YES
REM /L*V "%~dp02014SQLCMD32Bits.log"
START /WAIT "" msiexec.exe /i "%~dp02014SQLCMD32Bits.msi" /quiet /passive /qb /norestart IACCEPTMSSQLCMDLNUTILSLICENSETERMS=YES
SET "MISSING_TOOL=-1"
)

:WIN32_CONTINUE

ECHO !MISSING_TOOL! > "%~dp0ERRORLEVEL.txt"

GOTO END

:SQLCMD64

IF EXIST "%PROGRAMW6432%\Microsoft SQL Server\Client SDK\ODBC\130\Tools\Binn\SQLCMD.EXE" (
  
  SET "MISSING_TOOL=0"
  
  GOTO SQLCMD64_CONTINUE

)

IF EXIST "%PROGRAMW6432%\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn\SQLCMD.EXE" (
  
  SET "MISSING_TOOL=0"
  
  GOTO SQLCMD64_CONTINUE

)

IF EXIST "%PROGRAMW6432%\Microsoft SQL Server\110\Tools\Binn\SQLCMD.EXE" (

  SET "MISSING_TOOL=0"
  
  GOTO SQLCMD64_CONTINUE

)

IF EXIST "%PROGRAMW6432%\Microsoft SQL Server\100\Tools\Binn\SQLCMD.EXE" (

  SET "MISSING_TOOL=0"
  
  GOTO SQLCMD64_CONTINUE

)

IF %1 == 1 (
REM /L*V "%~dp02014ODBC64Bits.log"
START /WAIT "" msiexec.exe /i "%~dp02014ODBC64Bits.msi" /quiet /passive /qb /norestart IACCEPTMSODBCSQLLICENSETERMS=YES
REM /L*V "%~dp02014SQLCMD64Bits.log"
START /WAIT "" msiexec.exe /i "%~dp02014SQLCMD64Bits.msi" /quiet /passive /qb /norestart IACCEPTMSSQLCMDLNUTILSLICENSETERMS=YES
SET "MISSING_TOOL=-1"
)

:SQLCMD64_CONTINUE

ECHO !MISSING_TOOL! > "%~dp0ERRORLEVEL.txt"

GOTO END

:END 

REM SEGUIR AQUI... O TERMINAR.