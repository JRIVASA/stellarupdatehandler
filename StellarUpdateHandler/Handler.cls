VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 1  'Persistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TaskManager"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Property Let Parametros(pValor As Dictionary)
    Set Configuracion = pValor
End Property

Property Get Parametros() As Dictionary
    Set Parametros = Configuracion
End Property

Property Let LinkClass(ByVal pValor As Object)
    Set Link = pValor
End Property

Property Get LinkClass() As Object
    Set LinkClass = Link
End Property

Public Sub Inicializar()
    IniciarProceso
End Sub

Public Sub Detener()
    DetenerProceso
End Sub

Public Sub Finalizar()
    End
End Sub

Private Sub Class_Initialize()
    Set SharedCls = Me
End Sub
